$LOGFILE = "c:\logs\windows-stata.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

function download_extract ($name, $url, $extractpath, $hash, $unzip) {
  $fallback = $false
  if (!(Test-Path $extractpath)) {
      Write-Log "Create extact Folder $extractpath"
      New-Item -Path $extractpath -ItemType Directory -Force
  }

  Import-Module BitsTransfer
  Start-BitsTransfer -Source $url -Destination $extractpath -Asynchronous -DisplayName $name -TransferType Download

  while ((Get-BitsTransfer -Name $name).jobstate -eq "Connecting") {
      Start-Sleep -Seconds 1
      Write-Log "Connecting $url to download"
  }

  if ((Get-BitsTransfer -Name $name).jobstate -eq "Transferring") {
      Write-Log "downloading $name"
      while ((Get-BitsTransfer -Name $name).jobstate -eq "Transferring") { 
          Start-Sleep -Seconds 3 
      }
  
      if ((Get-BitsTransfer -Name $name).jobstate -eq "Transferred") {
          Write-Log "downloading $name is completed"
          Get-BitsTransfer -Name $name | Complete-BitsTransfer
      } else {
          $fallback = $true
      }
  } else {
      Write-Log "unable to download with BitsTransfer try with webrequest"
      $fallback = $true
  }

  if ($fallback) {
      #fallback download
      Invoke-WebRequest -Uri $url -OutFile $extractpath
  }

  #get filename
  $downloadfilename = Get-ChildItem $extractpath
  Write-Log "Downloaded file is $downloadfilename"

  if ($downloadfilename) {
      Write-Log "Download complete"
      
      #check if hash equals file
      # Calculate the SHA-256 hash of the file
      $calculatedHash = Get-FileHash -Path $downloadfilename.PSPath -Algorithm SHA256 | Select-Object -ExpandProperty Hash

      # Compare the calculated hash with the expected hash
      if ($calculatedHash -eq $hash) {
          Write-Log "The $name hash matches the expected hash."
          $result = 1
      } else {
          Write-Log "The $name hash ($calculatedHash) does not match the expected hash ($hash)."
          return 0
      }
  } else {
      Write-Log "Unable to download $url"
      $result = 0
  }

  if ($unzip) {
      Expand-Archive -Path $downloadfilename.PSPath -DestinationPath $extractpath
      $result = 1
  }

  return $result
}


Function Main {

  try {
    Write-Log "Start windows-stata"
    $downloadresult = download_extract -name "winscp" -url "https://sourceforge.net/projects/winscp/files/WinSCP/6.3.6/WinSCP-6.3.6-Setup.exe/download" -extractpath "$env:temp\winscp" -hash "63E86D50BD3E28B2F19D86114F589FF12C85CEBBA208E05ECE896BCF4DCE2AA2" -unzip $false
    if ($downloadresult -like "1") {

      Write-Log "install winscp"
      $downloadedfile = Get-ChildItem "$env:temp\winscp"

      if (($downloadedfile).name -eq "download") {
          Rename-Item -Path $downloadedfile.FullName -NewName winscp-setup.exe
          $setupfilename = "winscp-setup.exe"
      } else {
          $setupfilename = $downloadedfile.Name
      }
      Start-Process -FilePath "$env:temp\winscp\$setupfilename" -ArgumentList "/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP- /LANG=EN" -NoNewWindow -Wait
    } else {
      Write-Log "Unable to install the download check the log"
    }
  }
  catch {
      Write-Log "$_"
      Throw $_
  }


  Write-Log "End windows-stata"
}

Main    
